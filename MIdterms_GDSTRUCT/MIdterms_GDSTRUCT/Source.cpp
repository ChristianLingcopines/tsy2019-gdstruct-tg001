#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;
void systemclr()
{
	system("pause");
	system("cls");
}

void main()
{
	
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (true)
	{
		
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";

		int choice;
		cout << "\n\nWhat do you want to do?\n";
		cout << "\n1-Remove element at index \n2-Search for element \n3-Expand and generate random values\n\nEnter:";cin >> choice;

		if (choice == 1)
		{
			int index;
			
			cout << "\n Select an index:"; cin >> index;
			for (int i = 0; i < unordered.getSize(); i++)
			{
				if ((index) == i)
				{
					unordered.remove(index);
				}
			}
			
			for (int i = 0; i < ordered.getSize(); i++)
			{
				if ((index) == i)
				{
					ordered.remove(index);
				}
			}
				 if((index) >unordered.getSize() || (index) > ordered.getSize() )
				{
					cout << "\nElement not Found!!\n";
				}
			
		}

		else if (choice == 2)
		{
			cout << "\n\nEnter number to search: ";


			int input;
			cin >> input;
			cout << endl;

			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
			systemclr();
		}

		else if (choice == 3)
		{
			cout << "Input size of expansion: "; cin >> size;

			for (int i = 0; i < size; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			systemclr();
		}
	}
}